import argparse

import torch

import src

parser = argparse.ArgumentParser(description="Script to create and save a neural network")  # noqa
parser.add_argument("--path", help="Path of the model", required=True, type=str)  # noqa
parser.add_argument("--input-size-vector", help="Input size vector", required=True, type=str)  # noqa
parser.add_argument("--cuda", help="Using GPU or not", action="store_true")  # noqa

# Parse arguments
# --------------------
args = parser.parse_args()

# Using CUDA if asked and available
# --------------------
use_cuda = args.cuda and torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

# Creating and saving the neural network
# --------------------
model = src.Neural_Network_Meta_Classifier(
    int(args.input_size_vector)).to(device)

torch.save(model, args.path)
